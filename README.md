# Mars Rover Tech Challenge

## About The Project

This is my solution to the Mars Rover Tech Challenge.

Currently, the program does not come with a user interface. Because of this, I have relied on my unit tests to check that the program works as expected.

### Built With

* [Node.js](https://nodejs.org/en/)
* [Jest](https://jestjs.io/)


## Getting Started

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run the program, you must first install [Node.js](https://nodejs.org/en/).

### Installation

1. Clone this repository
   ```sh
   git clone https://gitlab.com/ianholden/mars-rover-tech-challenge.git
   ```
1. Install NPM packages
   ```sh
   npm install
   ```

## Useful Scripts

Once you have completed the installation steps, you should now be able to begin using the program.

### Start

Start the program.

```shell
npm start
```

### Test

Run Jest test suite. I have enabled coverage reporting by default.
```shell
npm run test
```

## Further Improvements

Given the description of this task, I wanted to try and complete this within the timeframe suggested (2-3 hours). Because of this, I wanted to highlight some of the additional features that I would add in future to improve my solution:

1. Use TypeScript to assist with type checking and provide a stricter working environment to (hopefully) discourage simple run-time errors.
1. Add further error checking to future-proof the system so that if a user interface were added, the system would not fall over if unexpected input was given. Some examples of further error checking would include:
	- Checking that worlds can only be represented by positive numbers.
	- Parsing instructions (using regex test) and checking that the format correlates with the expected format of an instruction string.
1. Use ES lint to assist with code consistency.
1. Use Prettier to assist with code formatting.
