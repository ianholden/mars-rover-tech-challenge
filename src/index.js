const Program = {}

Program.run = (input) => {
    if (typeof input !== 'string') throw new Error('Function: run parameter must be a string')

    const parsedInput = input.split('\n')
    const world = parsedInput[0]
    const robots = parsedInput.slice(1)
    const [maxM, maxN] = world.split(' ').map(str => Number(str))

    return robots.map(robot => Program.moveRobot(robot, { maxN, maxM })).join('\n')
}

Program.moveRobot = (instructions, {maxM, maxN}) => {
    if (typeof instructions !== 'string' || !Number.isInteger(maxM) || !Number.isInteger(maxN))
        throw new Error('Function: moveRobot parameters must be valid')

    const orientations = ['N', 'E', 'S', 'W']
    const [currentData, movementData] = instructions.slice(1).split(') ')
    const [m, n, currentOrientation] = currentData.split(', ')
    const parsedMovementData = movementData.split('')
    let intM = Number(m)
    let intN = Number(n)

    let newOrientationIndex = orientations.indexOf(currentOrientation)
    parsedMovementData.forEach(instruction => {
        if (instruction === 'L') return newOrientationIndex = (newOrientationIndex - 1 + orientations.length) % orientations.length
        if (instruction === 'R') return newOrientationIndex = (newOrientationIndex + 1) % orientations.length
        if (instruction === 'F') {
            if (orientations[newOrientationIndex] === 'N') return intM++
            if (orientations[newOrientationIndex] === 'S') return intM--
            if (orientations[newOrientationIndex] === 'E') return intN++
            if (orientations[newOrientationIndex] === 'W') return intN--
        }
    })

    const isLost = Program.isLost({ maxM, maxN }, { m: intM, n: intN })

    return `(${intM}, ${intN}, ${orientations[newOrientationIndex]})${isLost ? ' LOST' : ''}`
}

Program.isLost = ({maxM, maxN}, {m, n}) => {
    if (!Number.isInteger(maxM) || !Number.isInteger(maxN) || !Number.isInteger(m) || !Number.isInteger(n))
        throw new Error('Function: isLost parameters must be objects containing integers')

    return m > maxM || m < 0 || n > maxN || n < 0
}

module.exports = Program