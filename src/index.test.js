const Program = require('.')

describe('Program', () => {
  describe('Program.run', () => {
    describe('Error Handling', () => {
      test('throws if invalid parameter is passed to the function', () => {
        expect(() => Program.run()).toThrow();
        expect(() => Program.run(null)).toThrow();
        expect(() => Program.run(0)).toThrow();
        expect(() => Program.run({})).toThrow();
        expect(() => Program.run([])).toThrow();
      });
    })

    test('given input returns output as expected', () => {
      const input = `4 8
(2, 3, N) FLLFR
(1, 0, S) FFRLF`
      const expectedOutput = `(2, 3, W)
(-2, 0, S) LOST`
      expect(Program.run(input)).toBe(expectedOutput);
    });

    test('given input returns output as expected', () => {
      const input = `2 2
(0, 0, N) FFLLFR
(1, 0, N) FFLLFR
(2, 0, N) FFLLFR`
      const expectedOutput = `(1, 0, W)
(2, 0, W)
(3, 0, W) LOST`
      expect(Program.run(input)).toBe(expectedOutput);
    });
  })

  describe('Program.moveRobot', () => {
    describe('Error Handling', () => {
      test('throws if invalid parameters are passed to the function', () => {
        expect(() => Program.moveRobot()).toThrow();
        expect(() => Program.moveRobot(0, 0)).toThrow();
        expect(() => Program.moveRobot('', '')).toThrow();
        expect(() => Program.moveRobot([], [])).toThrow();
      });
  
      test('throws if instructions are not supplied to the function', () => {
        expect(() => Program.isLost(null, {maxM: 0, maxN: 0})).toThrow();
      });
  
      test('throws if current position is not supplied to the function', () => {
        expect(() => Program.isLost('', null)).toThrow();
      });
    })

    test('given input returns output as expected', () => {
      const instructions = `(2, 3, N) FLLFR`
      const world = { maxM: 4, maxN: 8 }
      const expectedOutput = `(2, 3, W)`
      expect(Program.moveRobot(instructions, world)).toBe(expectedOutput);
    });

    test('given input returns output as expected', () => {
      const instructions = `(1, 0, S) FFRLF`
      const world = { maxM: 4, maxN: 8 }
      const expectedOutput = `(-2, 0, S) LOST`
      expect(Program.moveRobot(instructions, world)).toBe(expectedOutput);
    });

    test('given input returns output as expected', () => {
      const instructions = `(0, 0, N) FFRFFRFFRFFR`
      const world = { maxM: 2, maxN: 2 }
      const expectedOutput = `(0, 0, N)`
      expect(Program.moveRobot(instructions, world)).toBe(expectedOutput);
    });

    test('given input returns output as expected', () => {
      const instructions = `(2, 2, N) FFLFFLFFLFFL`
      const world = { maxM: 2, maxN: 2 }
      const expectedOutput = `(2, 2, N)`
      expect(Program.moveRobot(instructions, world)).toBe(expectedOutput);
    });
  })

  describe('Program.isLost', () => {
    describe('Error Handling', () => {
      test('throws if invalid parameters are passed to the function', () => {
        expect(() => Program.isLost()).toThrow();
        expect(() => Program.isLost(0, 0)).toThrow();
        expect(() => Program.isLost('', '')).toThrow();
        expect(() => Program.isLost([], [])).toThrow();
      });
  
      test('throws if world is not supplied to the function', () => {
        expect(() => Program.isLost(null, {m: 0, n: 0})).toThrow();
        expect(() => Program.isLost({}, {m: 0, n: 0})).toThrow();
      });
  
      test('throws if current position is not supplied to the function', () => {
        expect(() => Program.isLost({maxM: 4, minM: 8}, null)).toThrow();
        expect(() => Program.isLost({maxM: 4, minM: 8}, {})).toThrow();
      });
    })

    test('returns true if coordinates exist within the world\'s coordinates', () => {
      expect(Program.isLost({maxM: 4, maxN: 8}, {m: 0, n: 0})).toBe(false);
      expect(Program.isLost({maxM: 4, maxN: 8}, {m: 2, n: 2})).toBe(false);
      expect(Program.isLost({maxM: 4, maxN: 8}, {m: 4, n: 8})).toBe(false);
    });

    test('returns false if coordinates exist outside of the world\'s coordinates', () => {
      expect(Program.isLost({maxM: 4, maxN: 8}, {m: -1, n: 0})).toBe(true);
      expect(Program.isLost({maxM: 4, maxN: 8}, {m: -4, n: -2})).toBe(true);
      expect(Program.isLost({maxM: 4, maxN: 8}, {m: 41, n: 8})).toBe(true);
      expect(Program.isLost({maxM: 4, maxN: 8}, {m: 41, n: 48})).toBe(true);
    });
  })
})